#ifndef AVOID_H
#define AVOID_H

#include <QWidget>
#include <string>

using namespace std;

/// 
/// Class that has the attributes of the obstacles such as:
/// * X, Y coordinates
/// * Name
/// * Setters and Getters
///
class Obstacle : public QWidget
{
    Q_OBJECT
public:
    explicit Obstacle(QWidget *parent = 0);
    int x_obstacle;
    int y_obstacle;
    string name;
    string getObs();
    void setObs(string);
    int getXObstacle();
    int getYObstacle();
    void setXObstacle(int);
    void setYObstacle(int);

};

#endif // AVOID_H
