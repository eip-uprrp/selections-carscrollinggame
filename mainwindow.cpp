#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

///
/// Default Constructor for the MainWindow. Some properties:
/// * ui: Allows the user to access members of the ui
/// * scene: Creates a new scene where the game will be "painted"
/// * game:  Creates a new game
/// * globalTimer: Creates a new timer
/// * score: Getes the number of the score in a string for future use
///
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    /*
     * Sets the ui and the scene for future use.
     */
    ui->setupUi(this);
    scene= new QGraphicsScene(this);
    scene->setSceneRect(QRectF(QPointF(0,0), QPointF(0,0)));

    /*
     * Places the scene in the main view and then we move mainView
     * so that it is centralized
     */
    ui->mainView->setScene(scene);
    ui->mainView->setAlignment((Qt::AlignLeft | Qt::AlignTop));
    ui->mainView->resize(550,345);

    /*
     * Here we create a new game and set the racer to red, also
     * we create a timer and add the game to the scene
     */
    game = new play;
    game->setCar("red");
    globalTimer = new QTimer;
    scene->addWidget(game);

    /*
     * Get the score and store it as a string, then we just
     * move around the buttons and the labels to make the
     * ui more aesthetically pleasing
     */

    QString score = QString::number(game->getScore());

    ui->scoreLabel->setText("<h3><b>Score: </b></h3>");
    ui->label->setText(score);

    ui->racerLabel->setText("Racer: ");
    ui->trackLabel->setText("Track: ");
    ui->difficultyLabel->setText("Difficulty: ");
    ui->retryButton->setEnabled(false);
    setWindowTitle("Scrolling Racing Game");
}

///
/// Default destructor
///
MainWindow::~MainWindow()
{
    delete ui;
    delete scene;
}

///
/// Starts the timer for the score and plays the game
/// using the run function from the game class
///
void MainWindow::on_playButton_clicked()
{
    game->run();
    connect(globalTimer,SIGNAL(timeout()),this,SLOT(score()));
    globalTimer->start(120);
    ui->playButton->setEnabled(false);
}

///
/// Sets the score of the game which is based on the timer
///
void MainWindow::score()
{
    if(game->playing == false) ui->retryButton->setEnabled(true);
    
    else ui->retryButton->setEnabled(false);
    
    QString num = QString::number(game->getScore());
    ui->label->setText(num);
}

///
/// When the retry button is pressed, the timer is
/// deleted and a new one is created also runs the
/// new game function from the class game.
///
void MainWindow::on_retryButton_clicked()
{
    game->newGame();
    delete globalTimer;
    globalTimer = new QTimer;
    connect(globalTimer,SIGNAL(timeout()),this,SLOT(score()));
    globalTimer->start(120);
}

///
/// Moves the racer up and down when the up, down arrows
/// are pressed
///
void MainWindow::keyPressEvent(QKeyEvent *event){
    game->keyPressEvent(event);
}

///
/// Sets which race car the player is using
///
void MainWindow::on_comboBox_activated(const QString &arg1)
{

    game->setCar(arg1.toStdString());
    repaint();
}

///
/// Sets which track the player is racing on
///
void MainWindow::on_trackBox_activated(const QString &arg1)
{

    play::Track track_type ;

    if(arg1 == "Day"){
       track_type = play::DAY ;
    }
    else if(arg1 == "Night"){
        track_type = play::NIGHT ;
    }
    else if(arg1 == "Beach"){
        track_type = play::BEACH ;
    }
    else{
        track_type = play::CANDYLAND ;
    }
    game->setTrack(track_type);
    repaint();
}

///
/// Sets the difficulty of the game and changes some
/// boolean variables acoordingly
///
void MainWindow::on_difficultyBox_activated(const QString &arg1)
{

    game->level = play::EASY ;
    if(arg1 == "Easy"){
        game->level = play::EASY;
    }
    else if(arg1 == "Medium"){
        game->level = play::MEDIUM;
    }

    else if(arg1 == "Hard"){
        game->level = play::HARD;
    }
}
