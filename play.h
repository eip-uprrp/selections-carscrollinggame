#ifndef PLAY_H
#define PLAY_H

#include <QTimer>
#include <QtGui>
#include <QtCore>
#include <QWidget>
#include <QPainter>
#include <string>
#include <vector>
#include "obstacle.h"
#include "flag.h"
#include "car.h"

using namespace std;

const int STEPS = 10 ;
const int CARX = 70 ;
const int FLAGRANGE = 20;
const int OBSTACLERANGE = 30 ;

class play : public QWidget
{
    Q_OBJECT
public:
    enum Difficulty {EASY, MEDIUM, HARD} ;
    enum Track {DAY, NIGHT, BEACH, CANDYLAND} ;

    explicit play(QWidget *parent = 0);

    int x1;
    int x2;

    int y_car;
    int score;
    Difficulty level ;
    bool playing;
    void collision();
    void keyPressEvent(QKeyEvent *event);
    void setScore(int = 1);
    void setCar(string);
    int getScore();
    void updateObs();
    void updateObstacleYCoordinate(int &, int &, int &, int &) ;
    string track;
    Track tt ;
    void setTrack(Track);
    void setTrackPixmap(Track) ;
    string getTrackPixmap() ;
    Track getTrack();
    void stopGame();


    ~play();

public slots:
    void mySlot();
    void run();
    void newGame();

protected:
    void paintEvent(QPaintEvent *);
    int randInt(int min, int max) ;
    void setObstaclesPixmap(string) ;
    bool obstacleCollision(Obstacle &, Car &) ;
    void flagCollision(Flag &, Car &);

    QTimer *myTimer;

    Obstacle obs[3];
    Flag flag;
    Car car;


};

#endif // PLAY_H
