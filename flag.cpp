#include "flag.h"

///
/// Default constructor. The properties for the checkpoints are set as follows:
/// * x_flag: x coordinates where the checkpoint will be placed
/// * y_flag: y coordinates where the checkpoint will be placed
/// * name:   Sets the image that will be used for the checkpoint
///
Flag::Flag(QWidget *parent) :
    QWidget(parent)
{
    x_flag = 250;
    y_flag = 140;
    name = ":/resources/flag.png";
}

///
/// Setter for the image of the checkpoint that will be used
///
void Flag::setFlag(string arg){
    name = arg;
}

///
/// Getter for the image of the checkpoint
///
string Flag::getFlag(){
    return name;
}

///
/// Setter for the X coordinates of the checkpoint
///
void Flag::setXFlag(int arg){
    x_flag = arg;
}

///
/// Setter for the Y coordinates of the checkpoint
///
void Flag::setYFlag(int arg){
    y_flag = arg;
}

///
/// Getter for the X coordinates of the checkpoint
///
int Flag::getXFlag(){
    return x_flag;
}

///
/// Getter for the Y coordinates of the checkpoint
///
int Flag::getYFlag(){
    return y_flag;
}

void Flag::hide(){
    setXFlag(630);
}
